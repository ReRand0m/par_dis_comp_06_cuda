#include <iostream>

__global__
void cuda_scan(int* input, int* res, unsigned int N) {
    res[0] = 0;
    for (int i = 1; i < N; ++i) {
        res[i] = input[i-1] + res[i-1];
    }
}

int main() {
    const unsigned int N = 1 << 22;

    cudaSetDevice (1);

    int *in = new int[N];
    int *res = new int[N];
    for (int i = 0; i < N; ++i)
        in[i] = 1;

    int* gpu_in;
    int* gpu_res;
    cudaMalloc(&gpu_in, sizeof(int) * N);
    cudaMalloc(&gpu_res, sizeof(int) * N);
    cudaMemcpy(gpu_in, in, sizeof(int) * N, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    cuda_scan<<<1, 1>>>(gpu_in, gpu_res, N);
    cudaEventRecord(stop);

    cudaMemcpy(res, gpu_res, sizeof(int) * N, cudaMemcpyDeviceToHost);
    cudaEventSynchronize(stop);

    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    std::cout << std::fixed << ms << "ms" << std::endl;
    bool f = true;
    for (int i = 0; i < N; ++i)
    {
        if (res[i] != i)
        {
            f = false;
            break;
        }
    }
    std::cout << ((f)? "ok" : "fail") << std::endl;

    cudaFree(gpu_in);
    cudaFree(gpu_res);
    delete[] in;
    delete[] res;

    return 0;
}
