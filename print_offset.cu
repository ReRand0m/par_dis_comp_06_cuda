#include <iostream>

#define BLOCK_SIZE 512

#define NUM_BANKS 32
#define LOG_NUM_BANKS 5

#define CONFLICT_FREE(n)  ((n) + ((n) >> LOG_NUM_BANKS ))

int main() {
    const unsigned int threads = BLOCK_SIZE;

    int(*(lmbds[]))(int, int) = {
            [] (int offset, int tid) { return offset*(2*tid+1)-1; },
            [] (int offset, int tid) { return CONFLICT_FREE(offset*(2*tid+1)-1); },
    };

    for (const auto& l : lmbds)
    {
        for (int offset = 1; offset < BLOCK_SIZE; offset*=2)
        {
            std::cout << offset << ") ";
            for (int tid = 0; tid < threads; ++tid)
            {
                if (tid < BLOCK_SIZE / offset)
                    std::cout << (l(offset, tid)) % 32 << "\t";
            }
            std::cout << std::endl;
        }
    }
    return 0;
}
