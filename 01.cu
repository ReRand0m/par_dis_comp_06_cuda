#include <iostream>

#define BLOCK_SIZE 512

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line)
{
    if (code != cudaSuccess)
        std::cerr << "\"" << cudaGetErrorString(code) << "\" in file " << file << ":" << line << std::endl;
}

__global__
void cuda_scan(int *input, int* output, int n) {
    __shared__ int sdata[BLOCK_SIZE];
    unsigned int tid = threadIdx.x;
    unsigned int igid = blockIdx.x * blockDim.x + tid;

    if (igid < n)
        sdata[tid] = input[igid];
    else
        sdata[tid] = 0;
    __syncthreads();

    for (int k = 1; k < blockDim.x; k*=2) {
        if (tid > k-1) {
            int prev = sdata[tid - k];
            __syncthreads();
            sdata[tid] += prev;
        }
        __syncthreads();
    }

    if (igid < n)
        output[igid] = sdata[tid];
}

int main() {
    const unsigned int N = 1 << 22;

    cudaSetDevice (1);

    int *in = new int[N];
    int *res = new int[N];
    for (int i = 0; i < N; ++i)
        in[i] = 1;

    int* gpu_in;
    int* gpu_res;
    cudaMalloc(&gpu_in, sizeof(int) * N);
    cudaMalloc(&gpu_res, sizeof(int) * N);
    cudaMemcpy(gpu_in, in, sizeof(int) * N, cudaMemcpyHostToDevice);

    int grid_size = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    cuda_scan<<<grid_size, BLOCK_SIZE>>>(gpu_in, gpu_res, N);
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    cudaEventRecord(stop);

    gpuErrchk( cudaMemcpy(res, gpu_res, sizeof(int) * N, cudaMemcpyDeviceToHost) );
    cudaEventSynchronize(stop);

    float ms;
    cudaEventElapsedTime(&ms, start, stop);
    std::cout << std::fixed << ms << "ms" << std::endl;
    bool f = true;
    for (int i = 0; i < N; ++i)
    {
        if (res[i] != (i % BLOCK_SIZE) + 1)
        {
            f = false;
            std::cout << i << " " << (i % BLOCK_SIZE) + 1 << " " << res[i] << std::endl;
            break;
        }
    }
    std::cout << ((f)? "ok" : "fail") << std::endl;

    cudaFree(gpu_in);
    cudaFree(gpu_res);
    delete[] in;
    delete[] res;

    return 0;
}
